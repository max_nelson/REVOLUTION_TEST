$(document).ready(function() {
  const grades = {
    '2': '2nd Grade',
    '3': '3rd Grade',
    '4': '4th Grade',
    '5': '5th Grade',
  };

  console.log('whatsup');
  let g = null;

  $('.name').keypress('change', function(e) {
    if (e.target.value.length < 40) {
      const chars = e.target.value.length;
      $('.vname').html(`${chars + 1} / 40`);
      g = 'this works too';
    }
  })

  $('.email').keypress('change', function(e) {
    if (e.target.value.length < 50) {
      const chars = e.target.value.length;
      $('.vemail').html(`${chars + 1} / 50`);
    }
  })

  $('span').on('click', function() {
    const grade = grades[$(this).attr('data')];
    $('.dropbtn').html(grade);
    g = $(this).attr('data')
    $('.dropbtn').css('border', '1px solid #888888');
  });

  $('#_submit').on('click', function() {
    const name = $('.name').val();
    const email = $('.email').val();
    if (name.length < 1) {
      $('.vname').css('color', '#952800');
    }

    if (email.length < 1) {
      $('.vemail').css('color', '#952800');
    }

    if (!g) {
      $('.dropbtn').css('border', '1px solid #952800');
    }
  })
})
